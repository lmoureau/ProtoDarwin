// This file is only useful for doxygen. There is no point `#include`ing it anywhere.

#error "Do not include this header (only useful for doxygen)"

////////////////////////////////////////////////////////////////////////////////
/// Global namespace for libraries and executables for analysis with Darwin.
namespace Darwin {

    ////////////////////////////////////////////////////////////////////////////////
    /// Everything what concerns physics analysis directly.
    namespace Physics {
        // nihil
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Classes and functions related to the framework.
    namespace Tools {
        // nihil
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Handling of exceptions.
    namespace Exceptions {
        // nihil
    }

}

////////////////////////////////////////////////////////////////////////////////
/// [Manual](https://root.cern/manual) [Doxygen](https://root.cern.ch/doc/master)
namespace ROOT {
    // nihil
}
