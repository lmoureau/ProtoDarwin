#ifndef DARWIN_LOOPER_H
#define DARWIN_LOOPER_H

#include <algorithm>
#include <memory>
#include <iostream>
#include <string>

#include <boost/exception/all.hpp>

#include <TChain.h>
#include <TTree.h>

#include "exceptions.h"

namespace Darwin::Tools {

typedef std::pair<unsigned, unsigned> Slice; //!< current slice (>=0) / total number of slices (>0)

////////////////////////////////////////////////////////////////////////////////
/// Prints the current slice and the total number of slices.
std::ostream& operator<< (std::ostream& Stream, const Darwin::Tools::Slice& slice)
{
    Stream << slice.second << '/' << slice.first;
    return Stream;
}

////////////////////////////////////////////////////////////////////////////////
/// Facility to loop over a n-tuple, including parallelisation and printing.
template<class T = TChain //!< either `TTree` or `TChain`
> class Looper {

    std::shared_ptr<T> tree; //!< tree
    const Slice slice; //! {#processes, process index}

    const long long nEvTot, //!< total number of entries in tree
                    start, //!< first entry (included)
                    stop, //!< last entry (excluded)
                    nEvSlice; // number of entries covered by the current process
    long long iEvSlice; // current entry
    long long percent; // percentage of progress for current entry

    void Init () const
    {
        using namespace std;

        if (slice.first < slice.second)
            BOOST_THROW_EXCEPTION(invalid_argument("The number of slices " + to_string(slice.first)
                        + " must be larger than the index of the current slice " + to_string(slice.second)));

        if (nEvSlice <= 0)
            BOOST_THROW_EXCEPTION(invalid_argument("The number of events cannot be " + to_string(nEvSlice)));

        cout << slice << '\t' << start << '-' << stop << endl;
    }

public:

    static long long division; //!< steps at which progress is printed (100%/division) 

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor for existing tree
    Looper (std::shared_ptr<T>& t, //!< n-tuple
            Slice s = {1,0} //!< exactly like the output of `Options::slice()`
            ) : tree(t), slice(s),
                nEvTot(t->GetEntries()),
                start(nEvTot*((s.second+0.)/s.first)),
                stop (nEvTot*((s.second+1.)/s.first)),
                nEvSlice(stop - start), iEvSlice(0), percent(-1)
    {
        Init();
        using namespace std;
        namespace DE = Darwin::Exceptions;
        string root_log = DE::intercept_printf([this]() { printf("loading first entry"); tree->GetEntry(start); });
        tree->GetEntry(start);
        if (root_log.find("Error") != string::npos)
            BOOST_THROW_EXCEPTION(runtime_error("Error while loading a TTree entry:\n" + root_log));
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor for simple counter
    Looper (long long nEvents, //!< total number of events to generate (with all slices)
            Slice s = {1,0} //!< exactly like the output of `Options::slice()`
            ) : tree(nullptr), slice(s),
                nEvTot(nEvents),
                start(nEvTot*((s.second+0.)/s.first)),
                stop (nEvTot*((s.second+1.)/s.first)),
                nEvSlice(stop - start), iEvSlice(0), percent(-1)
    {
        Init();
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Increments the counter and load entry (if applicable)
    void operator++ ()
    {
        ++iEvSlice;
        if (tree) tree->GetEntry(start + iEvSlice);
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Check that the counter is still in the range and prints percentage
    bool operator() ()
    {
        long long test_percent = (100ll*iEvSlice)/nEvSlice;

        if (test_percent != percent && test_percent % division == 0) {
            percent = test_percent;
            std::cout << slice.second << '/' << slice.first << '\t' << percent << '%' << std::endl;
        }

        return iEvSlice < nEvSlice;
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Pointer-like operator to return the value of the entry
    inline const long long operator* () const
    {
        return start + iEvSlice;
    }
};

template<class T> long long Looper<T>::division = 10;

}

using Darwin::Tools::operator<<;

#endif

