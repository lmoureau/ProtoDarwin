#ifndef DARWIN_METAINFO_H
#define DARWIN_METAINFO_H

#include <string>
#include <filesystem>
#include <memory>
#include <map>

#include "UserInfo.h"

struct git_repository;

namespace Darwin::Tools {

static std::ostream dev_null(nullptr); //!< to redirect output stream to nowhere

////////////////////////////////////////////////////////////////////////////////
/// \brief Generic meta-information for n-tuple (including speficities to Darwin).
///
/// Wrapper for `TList` information typically stored in `TTree::GetUserInfo()`.
/// The list includes information such as:
///  - a list of generic flags, e.g. the R parameter and jet algo (`flags`)
///  - some minimal software version information (`git`),
///  - the list of corrections applied so far (`corrections`),
///  - the successive commands run so far (`history`)
///  - and anything else that is possibly relevant (the format is flexible).
/// The information stored in the list can be converted into a config file
/// to reproduce identical results.
///
/// To initialise the meta information in a new `TTree`, a config with flags
/// is required:
/// ~~~cpp
/// MetaInfo metainfo(tree, config);
/// ~~~
/// To modify the meta information of an existing `TTree`, only the tree is 
/// necessary, but the config may be compared to the existing meta information:
/// ~~~cpp
/// MetaInfo metainfo(tree);
/// metainfo.Check(config);
/// ~~~
/// This is typically useful for reproducibility purposes, in the case where
/// the `config` was extracted from another `TTree` at a more advanced stage.
///
/// A typical use case is to determine generic flags for the data processing:
/// ~~~cpp
/// bool isMC = metainfo.Get<bool>("flags", "isMC");
/// int year = metainfo.Get<bool>("flags", "year");
/// ~~~
/// 
/// The `metainfo` can then be used like any other `UserInfo` instance.
/// Although one is entirely free to store any sort of information, it is important
/// to keep in mind the minimal structure of the file (see above) and respect it
/// as much as possible. For instance, to indicate an additional correction, the 
/// preferred way is the following:
/// ~~~cpp
/// metainfo.Set<string>("corrections", "nameOfMyCorrection", "value");
/// ~~~
class MetaInfo : public UserInfo {

    git_repository * repo; //!< pointer to local Git repo
    mutable int git_error; //!< C-style error code from Git operations 
                           //!< (see [source code](https://github.com/libgit2/libgit2/blob/v1.0.1/include/git2/errors.h))
    char sha[16]; //!< commit SHA at running time

    ////////////////////////////////////////////////////////////////////////////////
    /// Function to check if a file is ignored.
    ///
    /// \return EXIT_SUCCESS or EXIT_FAILURE
    static int get_one_status (const char *, //!< path to file
                       unsigned int, //!< status (necessary for `git_status_foreach` but unused)
                       void * //!< pointer to `git_repository`
                       );

    ////////////////////////////////////////////////////////////////////////////////
    /// Inits and opens git repo, sets `sha` to the value at running time.
    void GitInit ();

    ////////////////////////////////////////////////////////////////////////////////
    /// Mimicks `git status`.
    ///
    /// Adapted from 
    /// [this example](https://libgit2.org/libgit2/ex/v1.0.1/status.html).
    /// See also 
    /// [the documentation](https://libgit2.org/libgit2/#HEAD/group/status).
    void PrintStatus () const;

    ////////////////////////////////////////////////////////////////////////////////
    /// Function to check possibly uncommitted changes per file. It calls internally
    /// `MetaInfo::get_one_status` with a foreach loop.
    ///
    /// \return `true` if all changes have been committed
    bool AllChangesCommitted () const;

    ////////////////////////////////////////////////////////////////////////////////
    /// Update list with present git information. The `flags` and `git` blocks are
    /// expected to be found, otherwise it crashes smoothly. Then it updates the
    /// Git information.
    void UpdateList (const bool //!< should assume completeness (typically `false` for event loops)
                    ) const;

public:

    ////////////////////////////////////////////////////////////////////////////////
    /// Constsructor for first call ever, where the second parameter should be 
    /// extracted from the config file or from the command line.
    ///
    /// The preseed is taken from the config file *or* generated on the fly in a
    /// non-reproducible way. Use `getMetaInfo` for see how to force the seed
    /// to a value from the config file.
    MetaInfo (TTree *, //!< `TTree` whose `UserInfo` will be populated
              const boost::property_tree::ptree& //!< input config (must include flags)
             );

    ////////////////////////////////////////////////////////////////////////////////
    /// Constsructor for call of existing n-tuple. By default, the `git.complete`
    /// flag will be reset to `false`. It should be set to `true` after the event 
    /// loop in the executable.
    MetaInfo (TTree *, //!< `TTree` whose `UserInfo` will be modified
              bool = true //!< flag to init git repo
             );

    ////////////////////////////////////////////////////////////////////////////////
    /// Constsructor for direct call to a list (typically from a projection). By
    /// default, the `git.complete` will be kept as it is.
    MetaInfo (TList *, //!< `TList` that will be modified
              bool = true //!< flag to init git repo
             );

    ////////////////////////////////////////////////////////////////////////////////
    /// Generic constsructor for shared pointers
    template<typename T, typename ...Args>
    MetaInfo (const std::shared_ptr<T>& t, Args... args)
        : MetaInfo(t.get(), args...) { }

    ////////////////////////////////////////////////////////////////////////////////
    /// Destructor (releases memory & shuts down Git).
    ~MetaInfo ();

    ////////////////////////////////////////////////////////////////////////////////
    /// Compare the meta information with the config given in argument. Either 
    /// warnings or errors are thrown according to the severity of the inconsistencies.
    /// It also completes the records of successive commands.
    void Check (const boost::property_tree::ptree&) const;

    ////////////////////////////////////////////////////////////////////////////////
    /// \return string with libgit2 version.
    static std::string libgit2_version ();

    static std::map<std::string, std::string> versions; //!< software version

    ////////////////////////////////////////////////////////////////////////////////
    /// Generate the seed in a reproducible way:
    /// \f[
    ///    s = h + (k+1) \times b
    /// \f]
    /// where
    /// - $h$ is hard-coded in the program and should be different for every call 
    ///   of the functor,
    /// - $k$ corresponds to the (incremented) slice index,
    /// - and $b$ should be different for every sample (a.k.a. *preseed*).
    template<unsigned h //!< hard-coded component
    > unsigned Seed (std::pair<unsigned short, unsigned short> slice = {1,0} //!< variable part
                    ) const
    {
        unsigned b = Get<int>("preseed");
        unsigned k = slice.second;
        return h + (k+1) * b;
    }

    static const std::filesystem::path origin; //!< path to local repo, must be determined at compilation time
};

} // end of namespace

#endif
