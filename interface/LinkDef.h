#ifdef __CLING__

#pragma link C++ class Darwin::Physics::GenEvent +;
#pragma link C++ class Darwin::Physics::RecEvent +;
#pragma link C++ class Darwin::Physics::Trigger +;
//#pragma link C++ class Darwin::Physics::genMET +;
#pragma link C++ class Darwin::Physics::RecMET +;
#pragma link C++ class Darwin::Physics::PileUp +;
#pragma link C++ class Darwin::Physics::Vertex +;
#pragma link C++ class std::vector<Darwin::Physics::Trigger> +;

#pragma link C++ class Darwin::Physics::FourVector +;
#pragma link C++ class Darwin::Physics::Jet +;
#pragma link C++ class Darwin::Physics::GenJet +;
#pragma link C++ class Darwin::Physics::RecJet +;
#pragma link C++ class std::vector<Darwin::Physics::FourVector> +;
#pragma link C++ class std::vector<Darwin::Physics::GenJet> +;
#pragma link C++ class std::vector<Darwin::Physics::RecJet> +;

#endif
