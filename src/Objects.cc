#include "Objects.h"
#include <boost/exception/all.hpp>
#include <stdexcept>
#include <cmath>
#include <algorithm>

using namespace std;
using namespace Darwin::Physics;

void Event::clear()
{
    weights.resize(1);
    weights.front() = 1;
}

Event::Event () : weights(1,1) { }

/******************************************************************************/

size_t GenEvent::iWgt = 0;

float GenEvent::weight () const { return weights.at(iWgt); }

GenEvent::GenEvent () : process(0), hard_scale(0) { }

void GenEvent::clear ()
{
    Event::clear();
    process = 0;
    hard_scale = 0;
}

/******************************************************************************/

size_t RecEvent::iWgt = 0;

float RecEvent::weight () const { return weights.at(iWgt); }

RecEvent::RecEvent () : fill(0), runNo(0), lumi(0), evtNo(0) { }

void RecEvent::clear ()
{
    Event::clear();
    fill = 0;
    runNo = 0;
    lumi = 0;
    evtNo = 0;
}

/******************************************************************************/

void Trigger::clear ()
{
    Bit.clear();
    PreHLT.clear();
    PreL1min.clear();
    PreL1max.clear();
}

/******************************************************************************/

MET::MET () : SumEt(0) { }

void MET::clear ()
{
    SetR(0);
    SetPhi(0);
    SumEt = 0;
}

/******************************************************************************/

void RecMET::clear ()
{
    MET::clear();
    Bit.clear();
}

/******************************************************************************/

// these are the recommended values for Run-2 at 13 TeV
float PileUp::MBxsec = 69200 /* mb */,
      PileUp::MBxsecRelUnc = 0.046; // 4.6%

PileUp::PileUp () : rho(0), nVtx(0), trpu(0), intpu(0), pthatMax(0) { }

float PileUp::GetTrPU (const char v) const
{
    switch (v) {
        case '+': return trpu * (1+MBxsecRelUnc);
        case '-': return trpu * (1-MBxsecRelUnc);
        case '0': 
        default:  return trpu;
    }
}

float PileUp::GetIntPU (const char v) const 
{
    switch (v) {
        case '+': return intpu * (1+MBxsecRelUnc);
        case '-': return intpu * (1-MBxsecRelUnc);
        case '0':
        default:  return intpu;
    }
}

float PileUp::GetIntPU (TRandom3& r3, const char v) const 
{
    float trpu = GetTrPU(v);
    return r3.Poisson(trpu);
}

void PileUp::clear ()
{
    rho = 0;
	nVtx = 0;
	trpu = 0;
	intpu = 0;
	pthatMax = 0;
}

/******************************************************************************/

Vertex::Vertex () : chi2(0), ndf(0), fake(true) { }

void Vertex::clear ()
{
    SetRho(0);
    SetPhi(0);
    SetZ(0);
    chi2 = 0;
    ndf = 0;
    fake = true;
}

/******************************************************************************/

FourVector::FourVector (const ROOT::Math::PtEtaPhiM4D<float>& p4)
{
    SetPxPyPzE( p4.Px(),
                p4.Py(),
                p4.Pz(),
                p4.E ()  );
}

float FourVector::Rapidity () const { return 0.5*log((E()+Pz())/(E()-Pz())); }
float FourVector::AbsRap   () const { return std::abs(Rapidity()); }

void FourVector::clear ()
{
    SetCoordinates(0.,0.,0.,0.);
}

FourVector& FourVector::operator+= (const ROOT::Math::PtEtaPhiM4D<float>& other)
{
    SetPxPyPzE( Px() + other.Px(),
                Py() + other.Py(),
                Pz() + other.Pz(),
                E () + other.E ()  );
    return *this;
}

FourVector FourVector::operator+ (const ROOT::Math::PtEtaPhiM4D<float>& other) const
{
    FourVector v(*this);
    v += other;
    return v;
}

/******************************************************************************/

float Jet::R = 0.4;

Jet::Jet () : pdgID(0), nBHadrons(0), nCHadrons(0), scales(1,1), weights(1,1) { }

void Jet::clear ()
{
    FourVector::clear();
    pdgID = 0;
    nBHadrons = 0;
    nCHadrons = 0;
    weights.resize(1);
    weights.front() = 1;
    scales.resize(1);
    scales.front() = 1;
}

Jet& Jet::operator+= (const Jet& other)
{
    if (this->weights.size() != other.weights.size() ||
        this->scales.size() != other.scales.size())
        BOOST_THROW_EXCEPTION(length_error("These jets have incompatible content and therefore cannot be added together."));

    float pt0 = this->Pt();
    *dynamic_cast<FourVector*>(this) += other;

    for (size_t iWgt = 0; iWgt < weights.size(); ++iWgt)
        this->weights[iWgt] *= other.weights[iWgt];

    if (this->Pt() > 0)
    for (size_t iScl = 0; iScl < scales.size(); ++iScl) {
        float pt1 = pt0    *   this->scales[iScl],
              pt2 = other.Pt()*other.scales[iScl];
        this->scales[iScl] = (pt1+pt2) / this->Pt();
    }
    else fill(this->scales.begin(), this->scales.end(), 0.);

    this->nBHadrons += other.nBHadrons;
    this->nCHadrons += other.nCHadrons;

    return *this;
}

/******************************************************************************/

size_t GenJet::iWgt = 0,
       GenJet::iScl = 0; 

float GenJet::weight () const { return weights.at(iWgt); }
float GenJet::CorrPt  () const { return scales.at(iScl)*Pt(); }
float GenJet::CorrEta () const { FourVector p(*this); p.Scale(scales.at(iScl)); return p.Eta(); } 

void GenJet::clear ()
{
    Jet::clear();
}

GenJet GenJet::operator+ (const GenJet& j) const
{
    GenJet jet = *this;
    jet += j;
    return jet;
}

/******************************************************************************/

size_t RecJet::iWgt = 0, 
       RecJet::iScl = 0; 

float RecJet::weight () const { return weights.at(iWgt); }
float RecJet::CorrPt  () const { return scales.at(iScl)*Pt(); }
float RecJet::CorrEta () const { FourVector p(*this); p.Scale(scales.at(iScl)); return p.Eta(); } 

RecJet::RecJet () : area(2*M_PI*Jet::R) { }

void RecJet::clear ()
{
    Jet::clear();
    tags.clear();
    area = 2*M_PI*Jet::R;
}

RecJet RecJet::operator+ (const RecJet& j) const
{
    RecJet jet = *this;
    jet += j;
    return jet;
}

std::ostream& operator<< (std::ostream& Stream, const GenEvent& event)
{
    Stream << '(' << event.process << ',' << event.hard_scale << ')';
    return Stream;
}

std::ostream& operator<< (std::ostream& Stream, const RecEvent& event)
{
    Stream << '(' << event.fill << ',' << event.runNo << ',' << event.lumi << ',' << event.evtNo << ')';
    return Stream;
}

std::ostream& operator<< (std::ostream& Stream, const FourVector& v)
{
    Stream << '(' << v.Pt() << ',' << v.Eta() << ',' << v.Phi() << ')';
    return Stream;
}

std::ostream& operator<< (std::ostream& Stream, const Jet& jet)
{
    Stream << '(' << jet.CorrPt() << ',' << jet.CorrEta() << ',' << jet.Phi() << ')';
    return Stream;
}

