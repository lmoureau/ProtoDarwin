#include "UserInfo.h"

#include <boost/preprocessor/seq/elem.hpp>
#include <boost/preprocessor/seq/for_each.hpp>

#include <regex>
#include <string>
#include <iomanip>
#include <fstream>
#include <algorithm>
#include <functional>
#include <utility>

#include <boost/property_tree/info_parser.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include <boost/algorithm/string.hpp>

#include <TTree.h>

namespace pt = boost::property_tree;
namespace al = boost::algorithm;

#include "colours.h"

using namespace std;
namespace fs = filesystem;

using namespace Darwin::Tools;

TList * UserInfo::List (TList * mother, const char * key) const
{
    TObject * obj = mother->FindObject(key);
    if (obj == nullptr) {
        auto daughter = static_cast<TList *>(obj);
        daughter = new TList;
        daughter->SetName(key);
        mother->Add(daughter);
        return daughter;
    }
    else {
        TString classname = obj->ClassName();
        if (classname != "TList")
            BOOST_THROW_EXCEPTION(invalid_argument(key));
        auto daughter = dynamic_cast<TList *>(obj);
        return daughter;
    }
}

pt::ptree UserInfo::MkPtree (TList * list, int type) const
{
    assert(list);
    pt::ptree tree; // this tree will contain the list
    for (const auto& obj: *list) {
        const char * key = obj->GetName();
        const TString classname = obj->ClassName();
        if (classname == "TList") {
            auto sublist = dynamic_cast<TList *>(obj);
            pt::ptree child = MkPtree(sublist, type);
            tree.add_child(key, child);
        }
        else if (classname == "TObjString") {
            auto objs = dynamic_cast<TObjString *>(obj);
            const char * value = objs->GetString().Data();
            if (list->GetSize() == 1) 
                tree.put<const char *>("", value);
            else switch (type) {
                case XML: tree.add("item", value);
                          break;
                case INFO:
                case JSON:
                    {
                         pt::ptree node;
                         node.put("", value);
                         tree.push_back(make_pair("", node));
                    }
            }
        }
#define ARITHMETIC_TYPES (bool)(char)(char16_t)(char32_t)(wchar_t)(signed char)(short)(short int)(int)(long int)(long long int)(unsigned char)(unsigned short)(unsigned short int)(unsigned)(unsigned int)(unsigned long)(unsigned long int)(unsigned long long)(unsigned long long int)(float)(double)(long double)
#define ELSE_IF_TPARAM(r, data, TYPE) \
        else if (classname == "TParameter<" BOOST_PP_STRINGIZE(TYPE) ">") { \
            auto param = dynamic_cast<TParameter<TYPE>*>(obj); \
            tree.add<TYPE>(key, param->GetVal()); \
        }
        BOOST_PP_SEQ_FOR_EACH(ELSE_IF_TPARAM, _, ARITHMETIC_TYPES)
#undef ARITHMETIC_TYPES
#undef ELSE_IF_TPARAM
        else 
            cerr << orange << "Warning: " << classname << " is not handled by " << __func__ << ", therefore `" << key << "` will ignored.\n" << def;
    }
    return tree;
}

pt::ptree UserInfo::Write (const std::filesystem::path& p) const
{
    int type = INFO;
    if (p.extension() == ".json")
        type = JSON;
    else if (p.extension() == ".xml")
        type = XML;

    pt::ptree tree = MkPtree(type);

    switch (type) {
        case JSON:
        {
            stringstream ss;
            write_json(ss, tree);
            string str = ss.str();
            // by default, everything is a string
            // -> we remove quotation marks for numerics and booleans
            regex numbers("\\\"([0-9]+\\.{0,1}[0-9]*)\\\""), // note: no support for hex
                  booleans("\\\"(true|false)\\\"");
            str = regex_replace(str, numbers, "$1");
            str = regex_replace(str, booleans, "$1");
            al::replace_all(str, "\\/", "/");
            ofstream file(p.c_str());
            file << str;
            file.close();
            break;
        }
        case XML :
        {
            pt::ptree userinfo;
            userinfo.put_child("userinfo", tree); // there must be exactly one global key
            pt::xml_writer_settings<string> settings(' ', 2); // indentation with 2 spaces
            write_xml(p.c_str(), userinfo, std::locale(), settings);
            break;
        }
        case INFO:
        {
            stringstream ss;
            write_info(ss, tree);
            string str = ss.str();
            al::erase_all(str, "\"\"");
            ofstream file(p.c_str());
            file << str;
            file.close();
        }
    }

    return tree;
}

void UserInfo::Write () const
{
    TString name = l->GetName();
    if (name.IsNull()) name = "UserInfo";
    l->Write(name, TList::kSingleKey);
}

bool UserInfo::Empty () const
{
    return l->GetSize() == 0;
}

UserInfo::UserInfo (const char * name)
    : own(true), l(new TList)
{
    l->SetName(name);
}

UserInfo::UserInfo (TList * L)
    : own(false), l(L)
{
    if (L == nullptr)
        BOOST_THROW_EXCEPTION(invalid_argument("Invalid list given in argument."));
}

UserInfo::UserInfo (TTree * t)
    : own(false), l(t->GetUserInfo())
{
}

UserInfo::~UserInfo ()
{
    if (!own) return;
    delete l;
}

size_t hash<pt::ptree>::operator() (boost::property_tree::ptree const& tree) const noexcept
{
    stringstream ss;
    pt::write_info(ss, tree);
    return hash<string>{}(ss.str());
}

size_t hash<UserInfo>::operator() (UserInfo const& ui) const noexcept
{
    pt::ptree const& tree = ui.MkPtree();
    return hash<pt::ptree>{}(tree);
}

size_t hash<TTree*>::operator() (TTree * const& tree) const noexcept
{
    size_t seed = 0;
    auto combine = [&seed] (size_t value) {
        seed ^= value + 0x9e3779b9 + (seed<<6) + (seed>>2);
    };

    // from TNamed
    unsigned long Hash = tree->Hash();
    size_t h1 = hash<unsigned long>{}(Hash);
    combine(h1);

    hash<long long> HashL;

    long long b = tree->GetTotBytes();
    size_t h2 = HashL(b);
    combine(h2);

    long long N = tree->GetEntries();
    size_t h3 = HashL(N);
    combine(h3);

    const UserInfo ui(tree->GetUserInfo());
    size_t h4 = hash<UserInfo>{}(ui);
    combine(h4);

    return seed;
}
