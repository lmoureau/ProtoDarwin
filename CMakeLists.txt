# SPDX-License-Identifier: GPLv3-or-later
#
# SPDX-FileCopyrightText: Louis Moureaux <louis.moureaux@cern.ch>

cmake_minimum_required(VERSION 3.17 FATAL_ERROR)

# This has to be done before project()
if (DEFINED ENV{CMSSW_BASE})
    message(STATUS "CMS environment detected, setting up the toolchain")
    set(CMAKE_TOOLCHAIN_FILE "${CMAKE_SOURCE_DIR}/cmake/CMSSWToolchain.cmake"
        CACHE STRING "")
endif()


project(ProtoDarwin
        VERSION 1.0
        DESCRIPTION "Event loop based prototype framework"
        HOMEPAGE_URL https://protodarwin.docs.cern.ch/
        LANGUAGES CXX)


if ("${CMAKE_SOURCE_DIR}" STREQUAL "${CMAKE_BINARY_DIR}")
    message(FATAL_ERROR "${CMAKE_PROJECT_NAME} does not support in-source builds")
endif()

# Import vendored CMake modules
list(APPEND CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake")

# Set the location of build products
include(GNUInstallDirs)

# Dependencies
find_package(Boost 1.78 REQUIRED COMPONENTS program_options system)
find_package(Doxygen)
find_package(Git REQUIRED)
find_package(ROOT 6.24 REQUIRED)
find_package(LibGit2 REQUIRED)
enable_testing()

# These settings affect all compilations so we set them here
include_directories("${CMAKE_CURRENT_LIST_DIR}/interface")
add_compile_definitions("DARWIN=\"${CMAKE_CURRENT_LIST_DIR}\"")

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

# Documentation
if (DOXYGEN_FOUND)
    add_subdirectory(doc)
endif()

# Actual build definitions
add_subdirectory(src)
add_subdirectory(bin)
add_subdirectory(python)
add_subdirectory(scripts)
add_subdirectory(test)
