ROOTINC     ?= $(shell root-config --incdir)
ROOTCFLAGS  ?= $(shell root-config --cflags)
ROOTLDFLAGS ?= $(shell root-config --ldflags)
ROOTLIBS    ?= $(shell root-config --libs)

BOOST ?= /cvmfs/sft.cern.ch/lcg/views/LCG_100/x86_64-centos7-gcc9-opt
BOOSTINC  ?= $(BOOST)/include
BOOSTLIBS ?= $(BOOST)/lib

GITINC ?= /cvmfs/sft.cern.ch/lcg/views/LCG_100/x86_64-centos7-gcc9-opt/include
GITLIB ?= /cvmfs/sft.cern.ch/lcg/releases/libgit2/1.0.1-eb69e/x86_64-centos7-gcc9-opt/lib64/

EXAMPLES := example01 example02 example03
LIBS := Options UserInfo MetaInfo Objects
HEADERS := $(foreach HEADER,colours.h protodarwin.h Looper.h $(foreach LIB,$(LIBS),$(LIB).h),interface/$(HEADER))

COMMIT = $(shell git log -n 1 --pretty=format:'%H')
DIRS = $(foreach DIR,bin lib test tmp python doc,build/$(DIR))

CFLAGS = -g -Wall -O3 -std=c++17 -fPIC -DDEBUG -Iinterface -DDARWIN="\"$(PWD)\"" -DDARWIN_GIT_COMMIT="\"$(COMMIT)\"" -Wfatal-errors

.PHONY: all clean sweep doxygen firefox latex prefix

.SECONDARY:

all: $(foreach FILE, $(foreach EXEC,forceMetaInfo getMetaInfo printMetaInfo printEntries printBranches printDarwinSoftwareVersion,bin/$(EXEC)) $(foreach EXAMPLE,$(EXAMPLES),test/$(EXAMPLE)) test/Looper lib/libDarwinObjects.so lib/libDarwinDict.so test/main,build/$(FILE)) $(LIBS) prefix
	mkdir -p build
	cp test/examples test/example.info test/job test/transcribe build/test/
	cp .setup build/setup
	cd build/test && ./examples && ./transcribe

prefix:
	mkdir -p build
	cp -r python build
	mkdir -p build/bin
	find scripts -type f -perm /a+x -exec cp {} build/bin \;

build/test/main: build/tmp/testmain.o test/test.h build/lib/libDarwinDict.so build/lib/libDarwinObjects.so interface/Objects.h interface/exceptions.h $(foreach EXAMPLE,$(EXAMPLES),build/tmp/$(EXAMPLE).o) $(foreach LIB,$(LIBS),build/test/$(LIB)) $(foreach LIB,$(LIBS),build/lib/libDarwin$(LIB).so) build/test
	g++ ${CFLAGS} $(ROOTLDFLAGS) $< -o $@ -Itest $(ROOTLIBS) -Wl,-rpath,$(GITLIB) -L$(GITLIB) -lgit2 -L$(PWD)/build/lib -Wl,-rpath,$(PWD)/build/lib $(foreach LIB,$(LIBS),-lDarwin$(LIB)) -lDarwinDict -lDarwinObjects -L$(BOOSTLIBS) -lboost_timer -lboost_system -lboost_program_options -lrt -pthread -lresolv

build/test/example%: build/tmp/example%.o build/lib/libDarwinDict.so build/lib/libDarwinObjects.so interface/Objects.h $(foreach LIB,$(LIBS),build/test/$(LIB)) $(foreach LIB,$(LIBS),build/lib/libDarwin$(LIB).so) build/test
	g++ ${CFLAGS} $(ROOTLDFLAGS) $< -o $@ $(ROOTLIBS) -Wl,-rpath,$(GITLIB) -L$(GITLIB) -lgit2 -L$(PWD)/build/lib -Wl,-rpath,$(PWD)/build/lib $(foreach LIB,$(LIBS),-lDarwin$(LIB)) -lDarwinDict -lDarwinObjects -L$(BOOSTLIBS) -lboost_system -lboost_program_options

build/bin/forceMetaInfo: build/tmp/forceMetaInfo.o build/lib/libDarwinDict.so build/lib/libDarwinObjects.so interface/Objects.h $(foreach LIB,$(LIBS),build/test/$(LIB)) $(foreach LIB,$(LIBS),build/lib/libDarwin$(LIB).so) build/bin
	g++ ${CFLAGS} $(ROOTLDFLAGS) $< -o $@ $(ROOTLIBS) -Wl,-rpath,$(GITLIB) -L$(GITLIB) -lgit2 -L$(PWD)/build/lib -Wl,-rpath,$(PWD)/build/lib $(foreach LIB,$(LIBS),-lDarwin$(LIB)) -lDarwinDict -lDarwinObjects -Wl,-rpath,$(BOOSTLIBS) -L$(BOOSTLIBS) -lboost_system -lboost_program_options

build/bin/printMetaInfo: build/tmp/printMetaInfo.o build/lib/libDarwinDict.so build/lib/libDarwinObjects.so interface/Objects.h $(foreach LIB,$(LIBS),build/test/$(LIB)) $(foreach LIB,$(LIBS),build/lib/libDarwin$(LIB).so) build/bin
	g++ ${CFLAGS} $(ROOTLDFLAGS) $< -o $@ $(ROOTLIBS) -Wl,-rpath,$(GITLIB) -L$(GITLIB) -lgit2 -L$(PWD)/build/lib -Wl,-rpath,$(PWD)/build/lib $(foreach LIB,$(LIBS),-lDarwin$(LIB)) -lDarwinDict -lDarwinObjects -Wl,-rpath,$(BOOSTLIBS) -L$(BOOSTLIBS) -lboost_system -lboost_program_options

build/bin/getMetaInfo: build/tmp/getMetaInfo.o build/lib/libDarwinDict.so build/lib/libDarwinObjects.so interface/Objects.h $(foreach LIB,$(LIBS),build/test/$(LIB)) $(foreach LIB,$(LIBS),build/lib/libDarwin$(LIB).so) build/bin
	g++ ${CFLAGS} $(ROOTLDFLAGS) $< -o $@ $(ROOTLIBS) -Wl,-rpath,$(GITLIB) -L$(GITLIB) -lgit2 -L$(PWD)/build/lib -Wl,-rpath,$(PWD)/build/lib $(foreach LIB,$(LIBS),-lDarwin$(LIB)) -lDarwinDict -lDarwinObjects -Wl,-rpath,$(BOOSTLIBS) -L$(BOOSTLIBS) -lboost_system -lboost_program_options

build/bin/printDarwinSoftwareVersion: build/tmp/printDarwinSoftwareVersion.o $(foreach LIB,$(LIBS),build/test/$(LIB)) $(foreach LIB,$(LIBS),build/lib/libDarwin$(LIB).so) build/bin
	g++ ${CFLAGS} $(ROOTLDFLAGS) $< -o $@ $(ROOTLIBS) -Wl,-rpath,$(GITLIB) -L$(GITLIB) -lgit2 -L$(PWD)/build/lib -Wl,-rpath,$(PWD)/build/lib $(foreach LIB,$(LIBS),-lDarwin$(LIB)) -L$(BOOSTLIBS) -lboost_system -lboost_program_options

build/bin/printEntries: build/tmp/printEntries.o build/lib/libDarwinDict.so build/lib/libDarwinObjects.so interface/Objects.h $(foreach LIB,$(LIBS),build/test/$(LIB)) $(foreach LIB,$(LIBS),build/lib/libDarwin$(LIB).so) build/bin
	g++ ${CFLAGS} $(ROOTLDFLAGS) $< -o $@ $(ROOTLIBS) -Wl,-rpath,$(GITLIB) -L$(GITLIB) -lgit2 -L$(PWD)/build/lib -Wl,-rpath,$(PWD)/build/lib $(foreach LIB,$(LIBS),-lDarwin$(LIB)) -lDarwinDict -lDarwinObjects -L$(BOOSTLIBS) -lboost_system -lboost_program_options

build/bin/printBranches: build/tmp/printBranches.o build/lib/libDarwinDict.so build/lib/libDarwinObjects.so interface/Objects.h $(foreach LIB,$(LIBS),build/test/$(LIB)) $(foreach LIB,$(LIBS),build/lib/libDarwin$(LIB).so) build/bin
	g++ ${CFLAGS} $(ROOTLDFLAGS) $< -o $@ $(ROOTLIBS) -Wl,-rpath,$(GITLIB) -L$(GITLIB) -lgit2 -L$(PWD)/build/lib -Wl,-rpath,$(PWD)/build/lib $(foreach LIB,$(LIBS),-lDarwin$(LIB)) -lDarwinDict -lDarwinObjects -L$(BOOSTLIBS) -lboost_system -lboost_program_options

$(filter %,$(LIBS)): %: build/test/% build/lib/libDarwin%.so
	cd build/test && DARWIN_TABLES=build/test ./$@ -l all

build/test/Looper: build/tmp/testLooper.o interface/Looper.h
	g++ -g -Wall -Og -std=c++1z -fPIC -Itest -I$(BOOSTINC) $< -o $@ -L$(PWD)/build/lib -Wl,-rpath,$(PWD)/build/lib  -Wl,-rpath,$(BOOSTLIBS) -L$(BOOSTLIBS) -lboost_timer -lboost_system $(ROOTLIBS) -lrt -pthread -lresolv

build/test/%: build/tmp/test%.o build/lib/libDarwin%.so test/test.h $(HEADERS) build/test
	g++ -g -Wall -Og -std=c++1z -fPIC -Itest -I$(BOOSTINC) $< -o $@ -L$(PWD)/build/lib -Wl,-rpath,$(PWD)/build/lib -lDarwin$(subst build/test/,,$@) -Wl,-rpath,$(GITLIB) $(if $(filter $@,build/test/MetaInfo),-L$(GITLIB) -lgit2 -lDarwinUserInfo,)  -Wl,-rpath,$(BOOSTLIBS) -L$(BOOSTLIBS) -lboost_timer -lboost_system $(if $(filter $@,build/test/Options),-lboost_program_options,) $(ROOTLIBS) -lrt -pthread -lresolv

build/tmp/test%.o: test/%.cc $(HEADERS) build/tmp
	g++ ${CFLAGS} $(ROOTCFLAGS) -Itest -I$(BOOSTINC) -I$(ROOTINC) $< -o $@ -c

build/tmp/testLooper.o: test/Looper.cc interface/Looper.h build/tmp
	g++ ${CFLAGS} $(ROOTCFLAGS) -Itest -I$(BOOSTINC) -I$(ROOTINC) $< -o $@ -c

build/tmp/example%.o: test/example%.cc $(HEADERS) interface/exceptions.h interface/protodarwin.h build/tmp
	g++ ${CFLAGS} $(ROOTCFLAGS) -I$(BOOSTINC) -I$(ROOTINC) $< -o $@ -c

build/tmp/forceMetaInfo.o: bin/forceMetaInfo.cc $(HEADERS) interface/exceptions.h interface/protodarwin.h build/tmp
	g++ ${CFLAGS} $(ROOTCFLAGS) -I$(BOOSTINC) -I$(ROOTINC) $< -o $@ -c

build/tmp/printMetaInfo.o: bin/printMetaInfo.cc $(HEADERS) interface/exceptions.h interface/protodarwin.h build/tmp
	g++ ${CFLAGS} $(ROOTCFLAGS) -I$(BOOSTINC) -I$(ROOTINC) $< -o $@ -c

build/tmp/getMetaInfo.o: bin/getMetaInfo.cc $(HEADERS) interface/exceptions.h interface/protodarwin.h build/tmp
	g++ ${CFLAGS} $(ROOTCFLAGS) -I$(BOOSTINC) -I$(ROOTINC) $< -o $@ -c

build/tmp/printDarwinSoftwareVersion.o: bin/printDarwinSoftwareVersion.cc $(HEADERS) interface/exceptions.h interface/protodarwin.h build/tmp
	g++ ${CFLAGS} $(ROOTCFLAGS) -I$(BOOSTINC) -I$(ROOTINC) $< -o $@ -c

build/tmp/printEntries.o: bin/printEntries.cc $(HEADERS) interface/exceptions.h interface/protodarwin.h build/tmp
	g++ ${CFLAGS} $(ROOTCFLAGS) -I$(BOOSTINC) -I$(ROOTINC) $< -o $@ -c

build/tmp/printBranches.o: bin/printBranches.cc $(HEADERS) interface/exceptions.h interface/protodarwin.h build/tmp
	g++ ${CFLAGS} $(ROOTCFLAGS) -I$(BOOSTINC) -I$(ROOTINC) $< -o $@ -c

build/lib/libDarwinOptions.so: src/Options.cc interface/Options.h interface/colours.h build/lib
	g++ $(CFLAGS) -I$(BOOSTINC) -shared $< -L$(BOOSTLIBS) -lboost_system -lboost_program_options -o $@

build/lib/libDarwinMetaInfo.so: src/MetaInfo.cc interface/MetaInfo.h build/lib/libDarwinUserInfo.so interface/colours.h build/lib
	g++ $(CFLAGS) $(ROOTCFLAGS) -shared $< -I$(GITINC) -I$(BOOSTINC) -I$(ROOTINC) -o $@ -L$(GITLIB) -lgit2 -DDARWIN_VERSION="\"$(shell git log -n 1 --pretty=format:'%H')\""

build/lib/libDarwinUserInfo.so: src/UserInfo.cc interface/UserInfo.h interface/colours.h build/lib
	g++ $(CFLAGS) $(ROOTCFLAGS) -shared $< -I$(BOOSTINC) -I$(ROOTINC) $(ROOTLIBS) -o $@ 

build/lib/libDarwinObjects.so: src/Objects.cc interface/Objects.h build/lib
	g++ $(CFLAGS) $(ROOTCFLAGS) -shared $(filter %.cc,$^) -I$(BOOSTINC) -I$(ROOTINC) -o $@

build/lib/libDarwinDict.so: build/tmp/Dict.cc build/lib/Dict_rdict.pcm
	g++ $(CFLAGS) -I. $(ROOTCFLAGS) -shared $< -I$(ROOTINC) -o $@

build/lib/Dict_rdict.pcm: build/tmp/Dict.cc
	cp build/tmp/Dict_rdict.pcm build/lib/

build/tmp/Dict.cc: src/classes.h interface/LinkDef.h build/tmp build/lib
	rootcling -f $@ -p $(filter %.h,$^) -I interface

firefox: doxygen
	@nohup firefox build/doc/html/index.html > /dev/null 2>&1 &

latex: doxygen
	@nohup evince build/doc/latex/refman.pdf > /dev/null 2>&1 &

doxygen:
	echo "# Software\n" > VERSION.md
	build/bin/printDarwinSoftwareVersion | sed 's/^/ - /g' >> VERSION.md
	cd doc && doxygen Doxyfile
	make -C build/doc/latex

$(filter %,$(DIRS)): %:
	@mkdir -p $@

clean:
	@rm -rf build

sweep:
	@rm -rf build/tmp test/*root test/ntuple*/ test/hist*/
