#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <filesystem>
#include <string>
#include <optional>
#include <memory>

#include "protodarwin.h"
#include "Objects.h"

#include <boost/property_tree/ptree.hpp>      

#include <TROOT.h>
#include <TFile.h>
#include <TTree.h>
#include <TH1.h>

using namespace std;

namespace po = boost::program_options;
namespace pt = boost::property_tree;
namespace fs = filesystem;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace Darwin::Physics {

////////////////////////////////////////////////////////////////////////////////
/// Toy example to project a n-tuple.
void example03 (const vector<fs::path>& inputs, //!< input ROOT files (n-tuple)
                const fs::path& output, //!< output ROOT file (histograms)
                const pt::ptree& config, //!< config handled with `Darwin::Tools::Options`
                const int steering, //!< parameters obtained from explicit options 
                const DT::Slice slice = {1,0} //!< number and index of slice
                )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    shared_ptr<TChain> tIn = DT::GetChain(inputs);

    vector<RecJet> * recs = nullptr;
    tIn->SetBranchAddress("recs", &recs);

    unique_ptr<TFile> fOut(DT_GetOutput(output));
    auto tOut = shared_ptr<TTree>(tIn->CloneTree(0)); // ghost tree: only there to keep meta info

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);

    auto h = new TH1D("h", ";p_{T};N", 990, 10, 1000);

    // event loop
    for (DT::Looper looper(tIn, slice); looper(); ++looper) {
        static auto& cout = (steering & DT::verbose) == DT::verbose ? ::cout : DT::dev_null;
        for (const auto& rec: *recs) {
            h->Fill(rec.Pt());
            cout << rec << endl;
        }
        // here we do *NOT* fill the tree
    }

    metainfo.Set<bool>("git", "complete", true);

    tOut->Write();
    h->Write();

    cout << __func__ << ' ' << slice << " end" << endl;
}

} // end of namespace

namespace DP = Darwin::Physics;

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        vector<fs::path> inputs;
        fs::path output;

        DT::Options options("Toy example to project a n-tuple.", DT::split);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file");
        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DP::example03(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
