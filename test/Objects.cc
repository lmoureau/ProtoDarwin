#ifndef DOXYGEN_SHOULD_SKIP_THIS
#define BOOST_TEST_MODULE testObjects
#include <boost/test/included/unit_test.hpp>
#include <boost/exception/all.hpp>

#include "test.h"
#include "exceptions.h"
#include "Objects.h"

#include <limits>
#include <iostream>

using namespace std;

namespace DT = Darwin::Tools;
namespace DP = Darwin::Physics;
namespace DE = Darwin::Exceptions;

static const auto feps = numeric_limits<float>::epsilon();

BOOST_AUTO_TEST_SUITE( test_objects )

    BOOST_AUTO_TEST_CASE( dijets )
    {
        BOOST_TEST( std::abs(DP::Jet::R - 0.4) < feps );

        // just declare two dummy jets
        DP::RecJet jet1, jet2;
        BOOST_TEST( std::abs(jet1.area - 2*M_PI*0.4) < feps );
        jet1.SetPt(42);
        jet2.SetPt(24);
        jet1.scales = {1.1, 0.5, 1.7};
        jet2.scales = {1.1, 0.5, 1.7};
        jet1.nBHadrons = 1;
        jet2.nCHadrons = 2;

        // check that `Jet::CorrPt()` works as expected
        DP::RecJet::iScl = 1;
        BOOST_TEST( jet1.CorrPt() == 42*0.5 ); // note: we're comparing
        BOOST_TEST( jet2.CorrPt() == 24*0.5 ); // floats---can be instable...

        // check addition
        BOOST_REQUIRE_NO_THROW( jet1 + jet2 );
        DP::RecJet dijet = jet1 + jet2;
        BOOST_TEST( dijet.Pt() == 42 + 24 );
        DP::RecJet::iScl = 1;
        BOOST_TEST( dijet.scales.at(1) == 0.5 );
        BOOST_TEST( dijet.CorrPt() == (42+24)*0.5 );
        BOOST_TEST( dijet.weight() == jet1.weight() * jet2.weight() );
        BOOST_TEST( dijet.nBHadrons == 1 );
        BOOST_TEST( dijet.nCHadrons == 2 );

        cout << jet1 << '\t' << jet2 << '\t' << dijet << endl;

        jet1.clear();
        BOOST_TEST( jet1.Pt() == 0 );
        BOOST_TEST( jet1.pdgID == 0 );
        BOOST_TEST( jet1.nBHadrons == 0 );
        BOOST_TEST( jet1.nCHadrons == 0 );
        BOOST_TEST( std::abs(jet1.weights.front() - 1) < feps );
        BOOST_TEST( jet1.weights.size () == (size_t) 1 );
        BOOST_TEST( std::abs(jet1.scales.front() - 1) < feps );
        BOOST_TEST( jet1.scales.size () == (size_t) 1 );
#if BOOST_VERSION < 107600
        BOOST_REQUIRE_THROW( jet1 + jet2, boost::wrapexcept<length_error> );
#endif // TODO: fix... (note sure that the version is the problem)
    }

    BOOST_AUTO_TEST_CASE( genevents )
    {
        DP::GenEvent event;
        BOOST_TEST( std::abs(event.weights.front() - 1) < feps );
        BOOST_TEST( event.weights.size () == (size_t) 1 );
        BOOST_TEST( event.hard_scale == 0 );
        BOOST_TEST( event.process    == 0. );

        event.weights.front() = 42;
        event.weights.push_back(24);

        DP::GenEvent::iWgt = 1;
        BOOST_TEST( std::abs(event.weight() - 24) < feps );

        event.clear();
        BOOST_TEST( std::abs(event.weights.front() - 1) < feps );
        BOOST_TEST( event.weights.size () == (size_t) 1 );
        BOOST_TEST( event.hard_scale == 0 );
        BOOST_TEST( event.process    == 0. );

        cout << event << endl;
    }

    BOOST_AUTO_TEST_CASE( recevents )
    {
        DP::RecEvent event;
        BOOST_TEST( std::abs(event.weights.front() - 1) < feps );
        BOOST_TEST( event.weights.size () == (size_t) 1 );
        BOOST_TEST( event.fill == 0 );
        BOOST_TEST( event.runNo == 0 );
        BOOST_TEST( event.lumi == 0 );
        BOOST_TEST( event.evtNo == (unsigned long long) 0 );

        event.weights.front() = 42;
        event.weights.push_back(24);

        DP::RecEvent::iWgt = 1;
        BOOST_TEST( std::abs(event.weight() - 24) < feps );

        event.clear();
        BOOST_TEST( std::abs(event.weights.front() - 1) < feps );
        BOOST_TEST( event.weights.size () == (size_t) 1 );
        BOOST_TEST( event.fill == 0 );
        BOOST_TEST( event.runNo == 0 );
        BOOST_TEST( event.lumi == 0 );
        BOOST_TEST( event.evtNo == (unsigned long long) 0 );

        cout << event << endl;
    }

    BOOST_AUTO_TEST_CASE( pileup )
    {
        BOOST_TEST( std::abs(DP::PileUp::MBxsec - 69200 /* mb */) < feps );
        BOOST_TEST( std::abs(DP::PileUp::MBxsecRelUnc - 0.046) < feps ); // 4.6%

        DP::PileUp pileup;
        pileup.trpu = 42;
        pileup.intpu = 24;
        BOOST_TEST( pileup.GetTrPU() == 42 );
        BOOST_TEST( pileup.GetTrPU('+') > 42 );
        BOOST_TEST( pileup.GetTrPU('-') < 42 );
        BOOST_TEST( pileup.GetIntPU() == 24 );
        BOOST_TEST( pileup.GetIntPU('+') > 24 );
        BOOST_TEST( pileup.GetIntPU('-') < 24 );

        TRandom3 r1(23413), r2(45346);
        BOOST_TEST( pileup.GetIntPU(r1) != pileup.GetIntPU(r2));

        pileup.clear();
        BOOST_TEST( pileup.rho == 0 );
	    BOOST_TEST( pileup.nVtx == 0 );
	    BOOST_TEST( pileup.trpu == 0 );
	    BOOST_TEST( pileup.intpu == 0 );
	    BOOST_TEST( pileup.pthatMax == 0 );
    }

BOOST_AUTO_TEST_SUITE_END()

// TODO: test storage in `TTree`s

#endif
