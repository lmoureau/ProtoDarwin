#ifndef DOXYGEN_SHOULD_SKIP_THIS
#define DOXYGEN_SHOULD_SKIP_THIS
#define BOOST_TEST_MODULE testLooper
#include <boost/test/included/unit_test.hpp>
#include <boost/exception/all.hpp>

#include <TTree.h>
#include "Looper.h"

using namespace std;
using namespace Darwin::Tools;

BOOST_AUTO_TEST_SUITE( looper )

    BOOST_AUTO_TEST_CASE( test )
    {
        auto t = make_shared<TTree>("events", "events");
        Slice slice {10,5}; // 10 slices, take here the fifth, covering [50,60[

        // normal call
        BOOST_REQUIRE_NO_THROW( Looper(t, slice) );
        for (Looper looper(100ll, slice); looper(); ++looper)
            t->Fill();

        // normal call
        BOOST_REQUIRE_NO_THROW( Looper(t, slice) );

        // wrong order (5 slices, 10th slice)
        BOOST_REQUIRE_THROW( Looper(t, {5,10}), boost::wrapexcept<invalid_argument> );

        // too many slices (1000 > 100)
        BOOST_REQUIRE_THROW( Looper(t, {1000,0}), boost::wrapexcept<invalid_argument> );

        {
            Looper looper(t, slice);
            BOOST_TEST( *looper == 50 );
            BOOST_TEST( looper() );
            ++looper;
            BOOST_TEST( *looper == 51 );
        }

        long long i = 50;
        for (Looper looper(t, {10,5}); looper(); ++looper) {
            BOOST_TEST( *looper == i );
            ++i;
        }
    }

BOOST_AUTO_TEST_SUITE_END()

#endif
