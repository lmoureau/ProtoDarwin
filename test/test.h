#ifndef DARWIN_TEST_H
#define DARWIN_TEST_H

#include <iostream>
#include <vector>
#include <filesystem>
#include <string>
#include <utility>

namespace Darwin::Tools {

    ////////////////////////////////////////////////////////////////////////////////
    /// Tokenize a C-style string into a vector of C-style string with space as 
    /// delimiter. It is especially useful in tests where the command is hard-coded.
    inline std::vector<const char *> tokenize (const char * str)
    {
        std::cout << "\e[1m" << str << "\e[0m" << std::endl;

        // convert from `const char *` to `char *`
        char * cmd = new char[strlen(str)+1];
        strcpy(cmd, str);

        // tokenize
        std::vector<const char *> args;
        for (char * arg = strtok(cmd, " ");
                    arg != nullptr;
                    arg = strtok(NULL, " "))
            args.push_back(std::move(arg));

        //free(cmd); // do NOT free the memory
        return args;
    }

}

#endif
