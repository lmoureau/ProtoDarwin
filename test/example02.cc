#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <filesystem>
#include <string>
#include <optional>
#include <memory>

#include "protodarwin.h"
#include "Objects.h"

#include <boost/property_tree/ptree.hpp>      

using namespace std;

namespace pt = boost::property_tree;
namespace fs = filesystem;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace Darwin::Physics {

////////////////////////////////////////////////////////////////////////////////
/// Toy example to modify a n-tuple.
///
/// The template argument is only for the test of exceptions in unit tests.
template<bool DARWIN_TEST_EXCEPTIONS = false>
void example02 (const vector<fs::path>& inputs, //!< input ROOT files (n-tuples)
                const fs::path& output, //!< output ROOT file (n-tuple)
                const pt::ptree& config, //!< config handled with `Darwin::Tools::options`
                const int steering, //!< parameters obtained from explicit options 
                const DT::Slice slice = {1,0} //!< number and index of slice
                )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    shared_ptr<TChain> tIn = DT::GetChain(inputs);
    unique_ptr<TFile> fOut(DT_GetOutput(output));
    auto tOut = shared_ptr<TTree>(tIn->CloneTree(0));

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);

    vector<RecJet> * recs = nullptr;
    tIn->SetBranchAddress("recs", &recs);

    const auto& corrections = config.get_child("corrections");
    metainfo.Set<string>("corrections", "METfilters", corrections.get<string>("METfilters"));
    metainfo.Set<string>("corrections", "hotzones",   corrections.get<string>("hotzones"  ));

    metainfo.Set<const char *>("variations", "JECs", "nominal");
    if ((steering & DT::syst) == DT::syst) {
        metainfo.Set<const char *>("variations", "JECs", "JESdn"  );
        metainfo.Set<const char *>("variations", "JECs", "JESup"  );
    }

    for (DT::Looper looper(tIn, slice); looper(); ++looper) {
        static auto& cout = (steering & DT::verbose) == DT::verbose ? ::cout : DT::dev_null;

        if constexpr (DARWIN_TEST_EXCEPTIONS)
            if (*looper == 5)
                BOOST_THROW_EXCEPTION(DE::AnomalousEvent("Here is an anomally", tIn));

        for (auto& rec: *recs) {
            rec.area = M_PI*pow(0.4,2);
            rec.scales = {1.1}; // nominal
            if ((steering & DT::syst) == DT::syst) {
                rec.scales.push_back(1.05); // JESdn
                rec.scales.push_back(1.15); // JESup
            }
            cout << rec << endl;
        }

        if ((steering & DT::fill) == DT::fill) tOut->Fill();
    }

    metainfo.Set<bool>("git", "complete", true);
    tOut->Write();

    cout << __func__ << ' ' << slice << " end" << endl;
}

} // end of namespace

namespace DP = Darwin::Physics;

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        vector<fs::path> inputs;
        fs::path output;

        DT::Options options("Toy example to modify a n-tuple.", DT::config | DT::split | DT::fill | DT::syst);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file")
               .arg<fs::path>("hotzones"  , "corrections.hotzones"  , "location of hot zones in the calorimeter")
               .arg<fs::path>("METfilters", "corrections.METfilters", "location of list of MET filters to apply");
        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DP::example02(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
