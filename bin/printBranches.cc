#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <iomanip>
#include <filesystem>
#include <memory>
#include <array>

#include "protodarwin.h"

#include <TFile.h>
#include <TMemFile.h>
#include <TTree.h>
#include <TString.h>

using namespace std;

namespace fs = filesystem;

namespace DE = Darwin::Exceptions;

namespace Darwin::Tools {

////////////////////////////////////////////////////////////////////////////////
/// From an absolute number of bytes to human readable size in KB, MB, GB, etc.
template<int thousand = 1024> //!< https://en.wikipedia.org/wiki/Byte
const char * GetHumanReadableSize (long double size //!< size in bytes
                                  )
{
    static const array levels = {'\0', 'K', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y'};
    auto level = levels.begin();
    while (level != prev(levels.end()) 
            && size > static_cast<long double>(thousand)) {
        ++level;
        size /= thousand;
    }
    return Form("%.0Lf%c", size, *level);
}

////////////////////////////////////////////////////////////////////////////////
/// Adapted from [ROOT example](https://root.cern.ch/doc/master/printSizes_8C.html)
Long64_t GetSize (TBranch * br)
{
    TMemFile f("buffer", "CREATE");
    auto fcurrent = br->GetTree()->GetCurrentFile();
    if (fcurrent) {
        auto settings = fcurrent->GetCompressionSettings();
        f.SetCompressionSettings(settings);
    }
    f.WriteObject(br, "thisbranch");
    TKey * key = f.GetKey("thisbranch");
    Long64_t size = key->GetNbytes();

    Long64_t basket = br->GetZipBytes();
    if (basket == 0) basket = br->GetTotBytes();

    return basket + size;
}

////////////////////////////////////////////////////////////////////////////////
/// Adapted from [ROOT example](https://root.cern.ch/doc/master/printSizes_8C.html)
Long64_t loopBranches (TObjArray * branches, //!< list of (sub)branches
                       size_t factor, //!< correction factor for #files in `TChain`
                       int nIndent = 0
                       )
{
    size_t n = branches->GetEntries();
    Long64_t total = 0;
    for (size_t i = 0; i < n; ++i) {
        auto br = dynamic_cast<TBranch*>(branches->At(i));
        Long64_t size = GetSize(br);

        string branchname(nIndent, ' ');
        branchname += br->GetName();
        cout << left << setw(40) << branchname
             << right << setw(10) << GetHumanReadableSize(size * factor) << endl;

        TObjArray * subbranches = br->GetListOfBranches();
        size += loopBranches(subbranches, factor, nIndent + 1);
        total += size;
    }
    return total;
}

////////////////////////////////////////////////////////////////////////////////
/// Adapted from [ROOT example](https://root.cern.ch/doc/master/printSizes_8C.html)
void printBranches (vector<fs::path> inputs //!< input ROOT files
                   )
{
    inputs = GetROOTfiles(inputs);
    size_t factor = inputs.size();
    string location = GetFirstTreeLocation(inputs.front());
    shared_ptr<TChain> tIn = GetChain(inputs, location.c_str());
    TObjArray * branches = tIn->GetListOfBranches();
    size_t size = loopBranches(branches, factor);
    cout << right << setw(50) << GetHumanReadableSize(size * factor) << endl;
}

} // end of Darwin::Tools namespace

namespace DT = Darwin::Tools;

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        vector<fs::path> inputs;

        DT::Options options("Print a summary of the content of the first `TTree`s "
                            "found in any directory of the input ROOT files.");
        options.inputs("inputs", &inputs, "input ROOT files or directory");
        options(argc, argv);

        DT::printBranches(inputs);
        return EXIT_SUCCESS;
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }
}
#endif
