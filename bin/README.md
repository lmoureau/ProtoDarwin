# Utilities

## MetaInfo

To help handle the processing of the files, `getMetaInfo` produces a config file from the meta information of a series existing ROOT files. It also warns possible inconsistencies among ROOT files.

## ROOT built-in tools.

ROOT already provides commands to remove branches of a `TTree` in a `TFile` directly from the shell, namely `rootslimtree`, as well as additional command to explore and modify the content of a `TFile`, namely `rootcp`, `rootmv`, etc.
