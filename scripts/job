#!/usr/bin/env python3

import os, sys, prefix
from shutil import copy2, which
from pathlib import Path


def batchname(location) -> str:
    return location.replace('/', '_')[1:]


def run(cmds, inputs, output, args, other_jobs) -> str:
    """Prepares section in dag config. The name of the method is kept in
    analogy with the `submit` command. The name of the inputs and output are
    used to determine the job name. JOB and VARS are provided in a very
    similar way to `submit`; PARENT & CHILD are added depending on the 
    other jobs defined in the input dag."""

    if not "DARWIN_BASE" in os.environ:
        raise RuntimeError(
            "The $DARWIN_BASE environment variable has not been defined."
        )
    copy2(os.environ["DARWIN_BASE"] + "/python/.condor", output)
    copy2(os.environ["DARWIN_BASE"] + "/python/.run", output)

    output_batchname = batchname(output)
    print(output_batchname)
    node = f"JOB {output_batchname} {output}/.condor\n"
    node += f"VARS {output_batchname} "

    exec = Path(which(cmds.exec)).absolute()
    node += f"command=\"{exec}\" "

    if len(inputs) > 0:
        inputs_str = " ".join([str(input) for input in inputs])
        node += f"inputs=\"{inputs_str}\" "
    args_str = " ".join([str(arg) for arg in args])
    node += (
        f"outputDir=\"{output}\" args=\"{args_str}\" j=\"{cmds.nSplit}\"\n"
    )

    parents = []
    for input in inputs:
        input_batchname = batchname(input)
        if not input_batchname in other_jobs:
            continue
        parents += [input_batchname]
    if len(parents) > 0:
        parents_str = ' '.join(parents)
        parents_str = parents_str.strip('\\\"')
        node += f"PARENT {parents_str} CHILD {output_batchname}\n"

    return node;


def get_other_jobs(dag):
    """Extracts the name of the jobs from the input DAG."""

    p = Path(dag)
    if not p.exists():
        return []

    if dag == "/dev/stdout":
        print("\x1B[33mWarning [from {sys.argv[0]}]: `PARENT ... CHILD` blocks won't appear in the DAG\x1B[0m")
        return []

    if not os.access(p, os.W_OK):
        raise ValueError(dag + " is not writable")

    jobs = []
    with open(dag) as f:
        lines = f.read().splitlines()
        for line in lines:
            if len(line) == 0:
                continue
            words = line.split(' ')
            if words[0] != "JOB":
                continue
            jobs += [words[1]]

    return jobs

def main():
    """Implementation of `job` command, relying on homemade `prefix` lib."""

    cmds, args = prefix.preparse(
        sys.argv,
        tutorial=("Generate the description of a node in a HTCondor DAGMan file. "
                  "Only commands running over n-tuples may be prefixed, and must "
                  "natively output a ROOT file. Then you may use the full power "
                  "of HTCondor (https://htcondor.readthedocs.io/en/latest/)"),
        dag_opt=True
    )

    pref_job = prefix.PrefixCommand(sys.argv[0], cmds)

    if cmds.help:
        prefix.tweak_helper_multi(pref_job, multi_opt=False, dag_opt=True)

    if cmds.git:
        prefix.git_hash(cmds.exec)

    if cmds.help or cmds.git:
        sys.exit()

    pref_job.parse(args)
    pref_job.prepare_multi()

    # at this stage, `cmds.dag` is expected to be a directory (if it exists)
    cmds.dag.mkdir(parents=True, exist_ok=True)
    copy2(which(cmds.exec), cmds.dag)
    cmds.exec = cmds.dag / Path(which(cmds.exec)).name

    # from now on, `cmds.dag` is expected to be the DAG file itself
    cmds.dag /= "dag"
    other_jobs = get_other_jobs(cmds.dag)

    job = run(
        pref_job.cmds, pref_job.absinputs, str(pref_job.absoutput), pref_job.args, other_jobs
    )

    if cmds.verbose:
        print(f"Printing to {cmds.dag}")
    with open(cmds.dag, 'a') as dag:
        print(job, file = dag)


if __name__ == "__main__":
    #try:
        main()
    #except Exception as exception:
    #    prefix.diagnostic(exception)
    #    sys.exit(1)  # https://tldp.org/LDP/abs/html/exitcodes.html
