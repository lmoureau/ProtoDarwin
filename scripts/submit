#!/usr/bin/env python3

import os, subprocess, sys, prefix, time, signal, re
from shutil import copy2


def run(cmds, inputs, output, args) -> str:
    """Prepares and runs `condor_submit` command."""

    # remove slashes at the end
    while output[-1] == "/":
        output = output[:-1]
    batchname = (
        os.path.basename(cmds.exec)
        + "_"
        + os.path.basename(output)
        + "_"
        + time.strftime("%y%m%d_%H%M%S")
    )

    if not "DARWIN_BASE" in os.environ:
        raise RuntimeError(
            "The $DARWIN_BASE environment variable has not been defined."
        )
    copy2(os.environ["DARWIN_BASE"] + "/python/.condor", output)
    copy2(os.environ["DARWIN_BASE"] + "/python/.run", output)

    condor_submit = f"condor_submit -batch-name {batchname} command='{cmds.exec}' "
    if len(inputs) > 0:
        inputs_str = " ".join([str(input) for input in inputs])
        condor_submit += f"inputs={inputs_str} "
    args_str = " ".join([str(arg) for arg in args])
    condor_submit += (
        f"outputDir='{output}' args='{args_str}' j={cmds.nSplit} {output}/.condor"
    )
    print(condor_submit)

    if cmds.dry_run:
        sys.exit()
    subprocess.run(condor_submit, check=True, shell=True)

    return batchname


#def runAPI(cmds, inputs, output, args):
#    """Prepares and submit command. Not working at DESY, not tested at CERN."""
#
#    inputs_str = ""
#    if len(inputs) > 0:
#        inputs_str = "'" + " ".join([str(input) for input in inputs]) + "'"
#
#    args_str = " ".join([str(arg) for arg in args])
#
#    job = htcondor.Submit(
#        {
#            "universe": "vanilla",
#            "executable": f"{output}/.run",
#            "getenv": True,
#            "environment": f"LD_LIBRARY_PATH_CONDOR={os.environ['PWD']}/{output}:{os.environ['LD_LIBRARY_PATH']}",
#            "arguments": f"{cmds.exec} {inputs_str} {output}/$(ProcId).root {args_str} -k $(ProcId) -p submit",
#            "initialdir": os.environ["PWD"],
#            "transfer_executable": False,
#            "should_transfer_files": "NO",
#            "output": f"{output}/.$(ProcId).out",
#            "error": f"{output}/.$(ProcId).out",
#            "log": f"{output}/.$(ProcId).log",
#            "request_memory": "1024MB"
#            # TODO: job flavour: espresso
#        }
#    )
#
#    schedd = htcondor.Schedd()
#    result = schedd.submit(job, count=cmds.nSplit)
#
#    return result
# TODO: ensure that the htcondor package is installed


def queue(batchname, absoutput, nSplit) -> None:
    """Checks the last changes from the standard output."""

    condor_q = (f"condor_q -constraint 'JobBatchName == \"'{batchname}'\"' "
                "-af ClusterId ProcId JobStatus cmd Arguments")

    last_nums = [0] * (nSplit)
    reset = "\x1B[0m"

    # Clear previous outputs
    for k in range(nSplit):
        f_out = absoutput / f".{k}.out"
        if f_out.exists():
            f_out.unlink()

    while True:
        jobs = subprocess.check_output(
            condor_q + " | awk '{print $1,$2,$3,$4,$5}'", shell=True
        ).splitlines()

        n_remaining = len(jobs)
        n_running = sum([int(el.split()[2]) == 2 for el in jobs])

        if n_remaining == 0:
            print("submit done")
            return

        changed = False
        for k in range(nSplit):
            if not f_out.exists():
                continue
            with open(f_out) as myfile:
                f_list = list(myfile)
                formatting = ""
                for i in range(last_nums[k], len(f_list)):
                    changed = True
                    line = f_list[i].rstrip()

                    # find the special characters to encode the colour and the highlighting
                    ANSI = re.findall(r"\x1B(?:[@-Z\\-_]|\[[0-?]*[ -/]*[@-~])", line)

                    # if any was found, check if it should be reset to default
                    if len(ANSI) > 0 and ANSI[-1] == reset:
                        formatting = ""

                    # remove all closing series
                    while reset in ANSI:
                        ANSI = ANSI[ANSI.index(reset) + 1 :]

                    # make a string out of it
                    formatting += "".join(ANSI)

                    print(f"{reset}[{n_running}:{n_remaining}] {formatting}{line}")
                last_nums[k] = len(f_list)

        if not changed:
            print(f"[{n_running}:{n_remaining}]")

        time.sleep(10)


def main():
    """Implementation of `submit` command, relying on homemade `prefix` lib."""

    cmds, args = prefix.preparse(
        sys.argv,
        tutorial=("Runs the command using several jobs on local farm. Only commands "
                  "running over n-tuples may be prefixed, and must natively output  "
                  "a ROOT file. The prefix command will replace the single ROOT file "
                  "with a directory containing a series of ROOT files, as well as the "
                  "standard output stream in hidden text files. Unless required "
                  "otherwise, the command will monitor the running of the job until "
                  "completion."),
        multi_opt=True,
    )

    pref_submit = prefix.PrefixCommand(sys.argv[0], cmds)

    if cmds.help:
        prefix.tweak_helper_multi(pref_submit)

    if cmds.git:
        prefix.git_hash(cmds.exec)

    if cmds.help or cmds.git:
        sys.exit()

    pref_submit.parse(args)
    pref_submit.prepare_multi()
    pref_submit.prepare_fire_and_forget()

    batchname = run(
        pref_submit.cmds, pref_submit.inputs, pref_submit.output, pref_submit.args
    )

    if cmds.background:
        print("Check the status of your jobs with `condor_q`.")
        sys.exit()

    def handler(signum, frame):
        subprocess.run(
            "condor_rm -constraint 'JobBatchName == \"'" + batchname + "'\"'",
            shell=True,
        )
        sys.exit()

    signal.signal(signal.SIGINT, handler)

    queue(batchname, pref_submit.absoutput, pref_submit.cmds.nSplit)


if __name__ == "__main__":
    try:
        main()
    except Exception as exception:
        prefix.diagnostic(exception)
        sys.exit(1)  # https://tldp.org/LDP/abs/html/exitcodes.html
